import sitemap from '@astrojs/sitemap';
import svelte from '@astrojs/svelte';
import tailwind from '@astrojs/tailwind';
import astroLayouts from 'astro-layouts';
import remarkDescription from 'astro-remark-description';
import { defineConfig } from 'astro/config';
import { loadEnv } from 'vite';

import { parseEnv } from './src/lib/env';
import { apppendTrailingSlash, prependBaseUrl } from './src/lib/remarkLinks';

const layoutOptions = {
  'pages/**/*.md': '~/layouts/Prose.astro',
  'content/**/*.md': '~/layouts/Prose.astro',
  'content/posts/**/*.md': '~/layouts/Post.astro',
};

// Special loading of environment variables required in Astro config, see:
// https://docs.astro.build/en/guides/configuring-astro/#environment-variables
const env = parseEnv({
  ...import.meta.env,
  ...loadEnv(process.env.NODE_ENV, process.cwd(), ''),
});

// https://astro.build/config
export default defineConfig({
  site: env.SITE,
  base: env.BASE_URL,

  // Enforce consistent trailing slashes for all paths, required for the
  // sidebar to work correctly
  trailingSlash: 'always',
  markdown: {
    remarkPlugins: [
      [astroLayouts, layoutOptions],
      [remarkDescription, { name: 'excerpt' }],

      // Ensure all links have a trailing slash
      apppendTrailingSlash,

      // Ensure all links & images have the base URL prepended
      [prependBaseUrl, env],
    ],
  },
  integrations: [
    tailwind({
      // We do this manually via /src/styles/global.css
      applyBaseStyles: false,
    }),
    sitemap(),
    svelte(),
  ],
});
