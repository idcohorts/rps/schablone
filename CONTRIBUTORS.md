We thank all contributors of this project!

# Developers

- Jannes Höke
- Markus Katharina Brechtel (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne / University Hospital Frankfurt)

# AI Coding Support

The code and documentation was created with the help of AI Coding assistants:

- GitHub Copilot
- OpenAI ChatGPT
