export type Frontmatter = Record<string, unknown> & {
  title?: string;
  description?: string;
  layout?: string;
  order?: number;
};
