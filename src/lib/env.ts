import { z, ZodError } from 'zod';

const EnvBooleanSchema = z
  .enum(['true', 'yes', '1', 'false', 'no', '0'])
  .transform(value => ['true', 'yes', '1'].includes(value))
  .pipe(z.boolean());

export const EnvSchema = z.object({
  SITE_TITLE: z.string().default('Schablone'),
  SITE_DESCRIPTION: z.string().default('Opinionated static site generator image'),
  THEME: z.enum(['light', 'dark']).default('light'),

  USE_LOGO: EnvBooleanSchema.default('false'),

  USE_RPS_HEADER: EnvBooleanSchema.default('false'),
  RPS_HEADER_URL: z.string().default('/header.js'),

  USE_CMS: EnvBooleanSchema.default('false'),

  // From vite
  BASE_URL: z.string(),
  MODE: z.string(),
  DEV: z.boolean(),
  PROD: z.boolean(),
  SSR: z.boolean(),

  // From Astro
  ASSETS_PREFIX: z.string().optional(),
  SITE: z.string().optional(),
});

export type Env = z.infer<typeof EnvSchema>;

export function parseEnv(env: Record<string, string> = import.meta.env) {
  try {
    return EnvSchema.parse(env);
  } catch (error: unknown) {
    if (error instanceof ZodError) {
      console.error('Error parsing environment variables:', error.issues);
    } else {
      console.error('Error parsing environment variables:', error);
    }
    throw new Error('Error parsing environment variables');
  }
}

export const env = parseEnv();
