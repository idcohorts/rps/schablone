import type { RemarkPlugin } from '@astrojs/markdown-remark';
import type { Parent, RootContent } from 'mdast';

import { type Env } from './env';
import { joinPaths } from './path';

const walk = (node: Parent, transformer: (node: RootContent) => void) => {
  if (node.children) {
    for (const child of node.children) {
      transformer(child);
      walk(child as Parent, transformer);
    }
  }
};

const isFullUrl = (url: string) => {
  return url.startsWith('http://') || url.startsWith('https://') || url.startsWith('//');
};

const isUrlPath = (url: string, options?: { acceptRelative?: boolean }) => {
  options = { acceptRelative: false, ...options };
  const relative = options.acceptRelative ? url.startsWith('./') : false;
  return !isFullUrl(url) && (relative || url.startsWith('/'));
};

const isMediaUrl = (url: string) => {
  return url.startsWith('/media/');
};

export const apppendTrailingSlash: RemarkPlugin = () => {
  return tree => {
    walk(tree, node => {
      if (
        node.type === 'link' &&
        isUrlPath(node.url, { acceptRelative: true }) &&
        !isMediaUrl(node.url)
      ) {
        node.url = joinPaths(node.url, '/');
      }
    });
  };
};

export const prependBaseUrl: RemarkPlugin = (env: Env) => {
  return tree => {
    walk(tree, node => {
      if ('url' in node && isUrlPath(node.url, { acceptRelative: false })) {
        node.url = joinPaths(env.BASE_URL, node.url);
      }
    });
  };
};

export default apppendTrailingSlash;
