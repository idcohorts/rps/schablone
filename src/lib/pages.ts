import { getCollection, type ContentEntryMap } from 'astro:content';
import { env } from '~/lib/env';
import { joinPaths, trimSlashes } from './path';
import type { Frontmatter } from './types';

export function getFileName(path: string) {
  return path.split('/').at(-1)!;
}

export function removeLastPart(url: string) {
  return url.replace(/\/[^\/]*?\/?$/, '');
}

export function getPageForUrl(pages: Page[], url: string) {
  return pages.find(page => page.url === url);
}

export function stripBasePath(url: string, basePath: string = env.BASE_URL) {
  if (url.startsWith(basePath)) {
    return url.slice(basePath.length);
  }
  return url;
}

function rewriteUrl(url: string) {
  if (url === '') {
    return '/';
  } else if (url.endsWith('_index/')) {
    // "Fake" index files are used to provide frontmatter for directories
    return url.slice(0, -7);
  }
  return url;
}

type PageBase = {
  /** URL relative to the site root */
  url: string;
  /** The resolved, absolute path to the file. */
  file: string;
  /** The frontmatter context, if available. */
  frontmatter?: Frontmatter;
};

export type Page = PageBase & {
  /** The name of the file, with extension */
  filename: string;
  /** Best effort page title */
  title: string;
  /** True if detected to be an index file, ie. filename == index.(md|mdx|astro) */
  isIndex: boolean;
};

export function getPages(astroGlobResult: any): Page[] {
  const globResult = astroGlobResult as PageBase[];

  const pages = globResult
    // Ignore dynamic pages that don't render collections or pagination
    .filter(
      result =>
        !(
          result.url.includes('[') &&
          !result.frontmatter?.rendersCollection &&
          !result.frontmatter?.isPagination
        ),
    )
    .filter(result => !result.frontmatter?.hidden)
    .map(result => {
      const filename = getFileName(result.file);
      return {
        ...result,
        url: rewriteUrl(result.url),
        filename,
        title: result.frontmatter?.title?.toString() ?? filename,
        isIndex: result.file.match(/\/_?index\.(md|mdx|astro)$/i) !== null,
      };
    });

  // Ensure there are no two pages with the same URL, as this would cause
  // problems with the page tree.
  const pagesByPath = new Map<string, Page>();

  for (const page of pages) {
    if (pagesByPath.has(page.url)) {
      throw new Error(
        `Multiple pages exist with the same URL: ${page.url}\n${page.file}\n${
          pagesByPath.get(page.url)?.file
        }`,
      );
    }
    pagesByPath.set(page.url, page);
  }

  return pages;
}

type UrlPart = string;
export type PageTree = {
  tree: true;
  index?: Page;
  frontmatter?: Frontmatter;
  contents: Record<UrlPart, PageTree | Page>;
};

export async function buildPageTree(pages: Page[], basePath?: string) {
  if (basePath === undefined) {
    basePath = env.BASE_URL;
  }

  // Async reduce https://j5bot.medium.com/async-array-reduce-in-typescript-e116ba1f3578
  const tree = await pages.reduce(
    async (acc_: Promise<PageTree>, page: Page) => {
      const acc = await acc_;
      const url = trimSlashes(stripBasePath(page.url, basePath));
      const path = url.split('/');
      let current = acc;

      if (url === '') {
        current.index = page;
        return acc;
      }

      for (let i = 0; i < path.length; i++) {
        const part = path[i];
        if (i === path.length - 1) {
          if (page.isIndex) {
            if (!(part in current.contents)) {
              current.contents[part] = { tree: true, contents: {} };
            }
            // Stub _index.md files can be used to provide frontmatter for the
            // directory without creating a clickable link in the page tree.
            if (page.filename === '_index.md') {
              current.contents[part].frontmatter = page.frontmatter;
            } else {
              (current.contents[part] as PageTree).index = page;
            }
          } else {
            if (
              !!page.frontmatter?.rendersCollection &&
              typeof page.frontmatter.rendersCollection === 'string'
            ) {
              current.contents = {
                ...current.contents,
                ...(await buildDateTree(
                  page.frontmatter.rendersCollection as keyof ContentEntryMap,
                  removeLastPart(url),
                )),
              };
            } else if (page.frontmatter?.isPagination) {
              current.index = { ...page, url: joinPaths(removeLastPart(page.url), '1/') };
            } else {
              current.contents[part] = page;
            }
          }
        } else {
          if (!(part in current.contents)) {
            current.contents[part] = { tree: true, contents: {} };
          }
          current = current.contents[part] as PageTree;
        }
      }
      return acc;
    },
    Promise.resolve({ tree: true, contents: {} } as PageTree),
  );

  return tree;
}

export async function buildDateTree(
  collection: keyof ContentEntryMap,
  collectionBaseUrl: string,
): Promise<Record<string, PageTree>> {
  const entries = await getCollection(collection);
  const tree: Record<string, PageTree> = {};

  entries.sort((a, b) => {
    return (b.data.date ?? new Date(0)).getTime() - (a.data.date ?? new Date(0)).getTime();
  });

  for (const entry of entries) {
    const date = entry.data.date ?? new Date(0);
    const [year, month] = date.toISOString().split('-');
    const yearTree = tree[year] ?? {
      tree: true,
      frontmatter: { order: year },
      contents: {},
    };
    const monthTree = (yearTree.contents[month] ?? {
      tree: true,
      frontmatter: { order: -month },
      contents: {},
    }) as PageTree;

    monthTree.contents[entry.data.title ?? entry.slug] = {
      url: joinPaths(env.BASE_URL, collectionBaseUrl, entry.slug, '/'),
      file: entry.id,
      filename: entry.id,
      title: entry.data.title ?? entry.slug,
      isIndex: false,
      frontmatter: { ...entry.data, order: -date.getTime() },
    };

    yearTree.contents[month] = monthTree;
    tree[year] = yearTree;
  }

  return tree;
}

export function isTree(item: object): item is PageTree {
  return 'tree' in item && item.tree === true;
}

export function treeContains(tree: PageTree, path: string) {
  // recursively search the tree for the path
  const searchList = [...Object.entries(tree.contents)];
  if (tree.index) {
    searchList.unshift(['__index', tree.index]);
  }
  for (const [key, value] of searchList) {
    if (key === '__frontmatter') {
      continue;
    }
    if (isTree(value)) {
      if (treeContains(value, path)) {
        return true;
      }
    } else if (!isTree(value) && value.url === path) {
      return true;
    }
  }

  return false;
}

function getFrontmatter(item: PageTree | Page) {
  if (!isTree(item) || item.frontmatter) {
    return item.frontmatter;
  }

  const tree = item;

  if (tree.index) {
    return tree.index.frontmatter;
  }

  return undefined;
}

export function sortTreeLevel(tree: PageTree) {
  const treeEntries = Object.entries(tree.contents);

  // Gather the relevant frontmatter for each item in the tree
  const frontmattered = treeEntries
    // Prevent self-pollution on deeper levels
    .filter(([key, _]) => key !== '__frontmatter')
    // Collect frontmatter and beautify titles
    .map(([key, value]) => {
      const frontmatter = getFrontmatter(value);
      const title = frontmatter?.title ?? key;
      return [
        title,
        {
          ...value,
          __frontmatter: frontmatter,
        },
      ];
    }) as [
    string,
    (PageTree & { __frontmatter?: Frontmatter }) | (Page & { __frontmatter?: Frontmatter }),
  ][];

  // Sort the items
  frontmattered.sort(([aTitle, a], [bTitle, b]) => {
    const aOrder = a.__frontmatter?.order ?? 0;
    const bOrder = b.__frontmatter?.order ?? 0;
    // Allow for negative order values, so we can put stuff at the end
    if (aOrder !== 0 && bOrder !== 0) {
      return bOrder - aOrder;
    } else if (aOrder !== 0) {
      return -1 * aOrder;
    } else if (bOrder !== 0) {
      return 1 * bOrder;
    } else if (aTitle && bTitle) {
      return aTitle.localeCompare(bTitle);
    } else {
      return 0;
    }
  });

  return frontmattered;
}
