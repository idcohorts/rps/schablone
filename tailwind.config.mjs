import typography from '@tailwindcss/typography';
import animate from 'tailwindcss-animate';
import namedGridLines from '@savvywombat/tailwindcss-grid-named-lines';
import containerQueries from '@tailwindcss/container-queries';

/** @type {import('tailwindcss').Config} */
export default {
  darkMode: ['class'],

  content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],

  theme: {
    container: {
      center: true,
      padding: '2rem',
      screens: {
        '2xl': '1400px',
      },
    },
    extend: {
      // theming system based on shadcn/ui
      colors: {
        border: 'hsl(var(--border))',
        input: 'hsl(var(--input))',
        ring: 'hsl(var(--ring))',
        background: 'hsl(var(--background))',
        foreground: 'hsl(var(--foreground))',
        primary: {
          DEFAULT: 'hsl(var(--primary))',
          foreground: 'hsl(var(--primary-foreground))',
        },
        secondary: {
          DEFAULT: 'hsl(var(--secondary))',
          foreground: 'hsl(var(--secondary-foreground))',
        },
        destructive: {
          DEFAULT: 'hsl(var(--destructive))',
          foreground: 'hsl(var(--destructive-foreground))',
        },
        muted: {
          DEFAULT: 'hsl(var(--muted))',
          foreground: 'hsl(var(--muted-foreground))',
        },
        accent: {
          DEFAULT: 'hsl(var(--accent))',
          foreground: 'hsl(var(--accent-foreground))',
        },
        popover: {
          DEFAULT: 'hsl(var(--popover))',
          foreground: 'hsl(var(--popover-foreground))',
        },
        card: {
          DEFAULT: 'hsl(var(--card))',
          foreground: 'hsl(var(--card-foreground))',
        },
      },
      borderRadius: {
        lg: 'var(--radius)',
        md: 'calc(var(--radius) - 2px)',
        sm: 'calc(var(--radius) - 4px)',
      },

      // shadcn-vue animations
      keyframes: {
        'accordion-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-accordion-content-height)' },
        },
        'accordion-up': {
          from: { height: 'var(--radix-accordion-content-height)' },
          to: { height: 0 },
        },
        'collapsible-down': {
          from: { height: 0 },
          to: { height: 'var(--radix-collapsible-content-height)' },
        },
        'collapsible-up': {
          from: { height: 'var(--radix-collapsible-content-height)' },
          to: { height: 0 },
        },
      },
      animation: {
        'accordion-down': 'accordion-down 0.2s ease-out',
        'accordion-up': 'accordion-up 0.2s ease-out',
        'collapsible-down': 'collapsible-down 0.2s ease-in-out',
        'collapsible-up': 'collapsible-up 0.2s ease-in-out',
      },

      gridTemplateColumns: {
        'layout-lg':
          '[left] 1fr [sidebar-left] 14rem [sidebar-right gutter-left] 2rem [content-left] minmax(18rem, calc(768px - 4rem)) [content-right] 2rem [gutter-right] 1.5fr [right]',
        layout:
          '[left gutter-left sidebar-left] 1rem [content-left] 1fr [content-right] 1rem [sidebar-right gutter-right right]',
      },
      gridTemplateRows: {
        'layout-lg':
          '[top header-top] ' +
          'auto [header-bottom sidebar-top content-top] ' +
          'minmax(auto, 1fr) [content-bottom footer-top] ' +
          'auto [sidebar-bottom bottom]',
        layout:
          '[top header-top] ' +
          'auto [header-bottom sidebar-top] ' +
          'auto [sidebar-bottom content-top] ' +
          'minmax(auto, 1fr) [content-bottom footer-top] ' +
          'auto [bottom]',
      },
    },
  },

  plugins: [animate, typography, namedGridLines, containerQueries],
};
