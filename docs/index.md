# Schablone

Schablone is built with [Astro](https://astro.build/), [Typescript](https://www.typescriptlang.org/) and [Tailwind CSS](https://tailwindcss.com/).

# Purpose

The purpose of this project is to provide a simple way to render a collection of markdown documents into a static website. It includes a hierarchical navigation component that adapts to different directory structures.

# Features

- Render a hierarchy of markdown pages into a static website with a sidebar navigation
- Render a collection of blog posts organized by publication date
- Customizable light & dark themes
- Optional: Display a logo
- Optional: Display the RPS header
- Optional, work in progress: Provide a CMS for editing the markdown documents via Git gateway

# Usage

Schablone is distributed as a Docker image. To use it, create your own `Dockerfile` in your project directory similar to this:

```Dockerfile
FROM registry.gitlab.com/idcohorts/rps/schablone/releases:v0.0.12

# Copy over the pages, posts and static/media files
COPY pages/ /app/src/pages/
COPY posts/ /app/src/content/posts/
COPY public/ /app/public/


# General site settings
# ENV SITE="https://example.com"
# ENV BASE_URL="/docs/"
ENV SITE_TITLE="Beispieltitel"
ENV SITE_DESCRIPTION="Esse consectetur reprehenderit nulla exercitation."
ENV THEME="light"

# Feature toggles
ENV USE_LOGO=false

ENV USE_RPS_HEADER=false
ENV RPS_HEADER_URL="/header.js"

ENV USE_CMS=false


# RUN yarn build
# CMD yarn preview --host
CMD yarn dev --host
```

If you do not wish to use the CMS feature, you can remove the associated files by adding this line to your `Dockerfile` after copying your content:

```Dockerfile
RUN rm -rf /app/src/pages/decapcms.astro /app/src/pages/staticcms.astro
```

Similarly, if you do not wish to use a blog, you can remove the associated files by adding this line to your `Dockerfile` after copying your content:

```Dockerfile
RUN rm -rf /app/src/pages/posts/
```

These pages will not show up in the navigation even if they exist, however the sitemap will still contain them and they will be accessible via direct URL.

You can also use a `docker-compose.yml` file to run the container:

```yaml
services:
  schablone:
    build:
      context: .
      dockerfile: Containerfile
    ports:
      - '4321:4321'
    volumes:
      - ./pages/:/app/src/pages/
      - ./posts/:/app/src/content/posts/
      - ./public/media/:/app/public/media/
    #command: yarn preview --host
    command: yarn dev --host
```

## Pages

Pages are markdown files that are rendered into the website. They are placed in the `src/pages/` directory. The file name is used as the URL path. The frontmatter of the markdown file is used to set the title, layout and order of the page.

### Frontmatter

#### `title`

Sets the title of the page in the navigation.

#### `description`

Sets the description of the page.

#### `layout`

Sets the layout of the page. The default layout is `~/layouts/Prose.astro`.

#### `order`

Sets the order of the page in the navigation. Higher numbers are sorted towards the top, negative numbers towards the bottom. If the order is 0 (the default), the page is sorted alphabetically.

### Hierarchical Navigation

The navigation is built from the directory structure of the `src/pages/` directory. Directories are rendered as collapsible sections in the navigation.

The `index.md` file in a directory is used as the landing page of the directory and its frontmatter is used to set the title and order of the directory. If you want to have a directory without a landing page, you can create a `_index.md` file in the directory to specify the frontmatter.

A directory that only contains an `index.md` file is rendered as a single page without a collapsible section in the navigation.

## Posts

Posts are markdown files that are rendered as a blog. They are placed in the `src/content/posts/` directory. The file name is used as the URL path. The frontmatter of the markdown file is used to set the title, date and image of the post.

### Frontmatter

#### `title`

Sets the title of the post. This is displayed in the navigation, blog post list and as the title of the post page.

#### `date`

Sets the publication date of the post. This is displayed in the blog post list and the post page. It is also used to build the post hierarchy in the navigation.

#### `image`

Sets the image of the post. This is displayed in the blog post list and the post page.

## Static Files

Static files are placed in the `public/` directory. They are copied to the root directory of the website. The `public/media/` directory is used to store images if you are using the CMS feature.

## Customization

### Themes

The theme of the website can be set with the `THEME` environment variable. The available themes are `light` (the default) and `dark`. You can override theming variables by overwriting the `src/styles/custom/theme.css` file. The theming is inspired by [shadcn/ui](https://ui.shadcn.com/docs/theming#css-variables) using CSS variables with HSL colors.

### Logo

If the `USE_LOGO` environment variable is set to `true`, the logo is displayed at the top of the sidebar. Overwrite the `src/components/site/Logo.astro` file to customize the logo.

## RPS Header

If the `USE_RPS_HEADER` environment variable is set to `true`, the RPS header is displayed at the top of the website. The `RPS_HEADER_URL` environment variable can be used to set the URL of the RPS header script.

## CMS

If the `USE_CMS` environment variable is set to `true`, the CMS feature is enabled. This feature is still a work in progress and is not yet fully functional.

# Development

## Releasing a new version

To release a new version, create a new tag and push it to the repository:

```bash
git tag -m "Release v0.1.0" v0.1.0
git push origin v0.1.0
```
